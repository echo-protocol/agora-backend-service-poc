const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const multer = require('multer'); // v1.0.5
const upload = multer(); // for parsing multipart/form-data

const registration =  require('./services/registration');
const proposals = require('./services/proposals');
const votes = require('./votes');

app.use(bodyParser.json()); // for parsing application/json

app.post('/register', upload.array(), function (req, res) {

  if(!registration.checkRegistrationKey(req.body.registrationKey)){
    res.status(401).end();
    return
  }

  const userId = registration.registerUser(req.body.password);

  res.set('Content-Type', 'application/json');

  res.send({
    userId: userId,
  });

  console.log('New user registered: ' + userId)
});

app.post('/proposal', upload.array(), function (req, res) {

  if(!registration.authenticateUser(req.body.userId, req.body.password)){
    res.status(401).end();
    return
  }

  const propId = proposals.newProposal(req.body.proposal, req.body.choices, req.body.upvotesNeeded, req.body.voteStart, req.body.voteDuration);

  res.set('Content-Type', 'application/json');

  res.send({
    propId: propId,
  });

  console.log('New proposal registered: ' + propId)
});

app.get('/proposal', upload.array(), function (req, res) {

  if(!registration.authenticateUser(req.query.userId, req.query.password)){
    res.status(401).end();
    return
  }

  let proposal = {};

  proposal[req.query.propId] = proposals.proposals[req.query.propId];

  res.set('Content-Type', 'application/json');

  res.send(proposal);

});

app.post('/upvote', upload.array(), async function (req, res) {

  if(!registration.authenticateUser(req.body.userId, req.body.password)){
    res.status(401).end();
    return
  }

  if(!proposals.proposals[req.body.propId]){
    res.status(503).end();
    return
  }

  if(!proposals.proposals[req.body.propId].status){
    proposals.proposals[req.body.propId].upvotes += 1;

    if(proposals.proposals[req.body.propId].upvotes >= proposals.proposals[req.body.propId].upvotesNeeded){
      proposals.proposals[req.body.propId].status = true;
      // const voteId = await votes.deployVote(req.body.propId, proposals.proposals[req.body.propId]);
      proposals.proposals[req.body.propId].voteId = await votes.deployVote(req.body.propId, proposals.proposals[req.body.propId]);

      res.send(proposals.proposals[req.body.propId]);

      return
    }

  }

  res.send(proposals.proposals[req.body.propId]);

  console.log('New upvote for proposal ' + req.body.propId)
});

app.listen(3000, () => {
  console.log('Agora Backend Service Listening on port 3000\n')
});
