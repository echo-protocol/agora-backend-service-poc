require('dotenv').config();

const registration = require('./services/registration');
const elgamal = require('./services/elgamal');
const request = require('./utils/request');

const ballotBoxServerURI = 'http://' + process.env.BALLOT_BOX_SERVER_URI;

exports.deployedVotes = {};

exports.deployVote = async function(propId, proposal){

  const keyPair = await elgamal.generateKeyPairAsync();

  const pubKey = { p: keyPair.pBigInt.toString(), g: keyPair.gBigInt.toString(), A: keyPair.ABigInt.toString() };

  const vote = {
    serverKey: process.env.AGORA_SERVICE_KEY,
    pubKey,
    proposal: proposal.proposal,
    choicesCount: proposal.choices.length,
    voteStart: proposal.voteStart,
    voteDuration: proposal.voteDuration,
    authorizedVoters: registration.registeredUsers
  };

  let voteInfo = await request.sendPOSTRequest(ballotBoxServerURI  + '/vote', vote);

  vote['propId'] = propId;
  vote['voteId'] = voteInfo.voteId;
  vote['choicesList'] = voteInfo.choicesList;
  vote['decryptionKey'] = keyPair.skBigInt;

  exports.deployedVotes[voteInfo.voteId] = vote;

  scheduleTally(voteInfo.voteId, proposal.voteStart, proposal.voteDuration);

  // console.log('Vote deployed: ' + JSON.stringify(vote));

  console.log('Vote deployed ' + voteInfo.voteId + '\n');

  return voteInfo.voteId

};

function scheduleTally(voteId, voteStart, voteDuration){

  let delay  = voteStart + voteDuration - Date.now();

  setTimeout(() => {

    sendDecryptionKey(voteId)

  }, delay)
}

function sendDecryptionKey(voteId){

  const body = {
    serverKey: process.env.AGORA_SERVICE_KEY,
    voteId,
    decryptionKey: exports.deployedVotes[voteId].decryptionKey.toString()
  };

  request.sendPOSTRequest(ballotBoxServerURI + '/tally', body);

  console.log('Sent deryption key to Ballot Box Server')
}

