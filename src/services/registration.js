const uuidv4 = require('uuid/v4');
const SHA3 = require('../utils/sha3');
require('dotenv').config();

exports.registeredUsers = {};

exports.checkRegistrationKey = function(key){
  return key === process.env.REGISTRATION_KEY
};

exports.registerUser = function(password){
  const userId = uuidv4();
  exports.registeredUsers[userId] = { status: true, pwdHash: SHA3.sha3(password) };
  return userId
};

exports.authenticateUser = function(userId, password){
  if(!exports.registeredUsers[userId]) return false;
  return (exports.registeredUsers[userId].status && exports.registeredUsers[userId].pwdHash === SHA3.sha3(password))
};
