const uuidv4 = require('uuid/v4');

exports.proposals = {};

exports.newProposal = function(proposal, choices, upvotesNeeded, voteStart, voteDuration){

  const propId = uuidv4();

  exports.proposals[propId] = {status: false, proposal, choices, upvotesNeeded, voteStart, voteDuration, upvotes: 0, privateKey: null };

  return propId

};
